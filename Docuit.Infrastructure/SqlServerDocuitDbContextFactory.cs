﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Docuit.Infrastructure
{
    public class SqlServerDocuitDbContextFactory : IDesignTimeDbContextFactory<DocuitDbContext>
    {
        public DocuitDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DocuitDbContext>();
            var connectionString = "Server=THE-UNRULY\\SQLEXPRESS;Database=Docuit;Trusted_Connection=True;";

            builder.UseSqlServer(connectionString);

            return new DocuitDbContext(builder.Options);
        }
    }
}