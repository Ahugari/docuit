﻿using Docuit.Core.Models;
using Docuit.Core.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Infrastructure.Services
{
    public class EfCoreDocumentTypeStore : IDocumentTypeStore
    {
        private readonly IDbContextFactory<DocuitDbContext> _dbContextFactory;

        public EfCoreDocumentTypeStore(IDbContextFactory<DocuitDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<DocumentType> GetType(string Id, CancellationToken cancellationToken = default)
        {
            await using var dbContext = _dbContextFactory.CreateDbContext();
            return await dbContext.DocumentTypes.FirstOrDefaultAsync(x => x.Id == Id, cancellationToken);
        }

        public async Task<IEnumerable<DocumentType>> ListAsync(CancellationToken cancellationToken = default)
        {
            await using var dbContext = _dbContextFactory.CreateDbContext();
            return await dbContext.DocumentTypes.ToListAsync(cancellationToken);
        }
    }
}