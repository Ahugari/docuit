﻿using Docuit.Core.Models;
using Docuit.Core.Services;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Infrastructure.Services
{
    public class EfCoreDocumentStore : IDocumentStore
    {
        private readonly IDbContextFactory<DocuitDbContext> _dbContextFactory;

        public EfCoreDocumentStore(IDbContextFactory<DocuitDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<Document> GetAsync(string Id, CancellationToken cancellationToken = default)
        {
            await using var dbContext = _dbContextFactory.CreateDbContext();
            return await dbContext.Documents.FirstOrDefaultAsync(x => x.Id == Id, cancellationToken);
        }

        public async Task SaveAsync(Document entity, CancellationToken cancellationToken = default)
        {
            await using var dbContext = _dbContextFactory.CreateDbContext();
            var existingDocument = await dbContext.Documents.FirstOrDefaultAsync(x => x.Id == entity.Id, cancellationToken);

            if (existingDocument == null)
                await dbContext.Documents.AddAsync(entity, cancellationToken);
            else
                dbContext.Entry(existingDocument).CurrentValues.SetValues(entity);

            await dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}