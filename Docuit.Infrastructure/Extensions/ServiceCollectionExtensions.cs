﻿using Docuit.Core.Services;
using Docuit.Infrastructure.HostedServices;
using Docuit.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Docuit.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainPersistence(this IServiceCollection services, string connectionString)
        {
            var migrationsAssemblyName = typeof(SqlServerDocuitDbContextFactory).Assembly.GetName().Name;

            return services
                .AddPooledDbContextFactory<DocuitDbContext>(x => x.UseSqlServer(connectionString, db => db.MigrationsAssembly(migrationsAssemblyName)))
                .AddSingleton<IDocumentStore, EfCoreDocumentStore>()
                .AddSingleton<IDocumentTypeStore, EfCoreDocumentTypeStore>()
                .AddHostedService<RunMigrations>();
        }
    }
}