﻿using Elsa;
using Elsa.ActivityResults;
using Elsa.Attributes;
using Elsa.Expressions;
using Elsa.Models;
using Elsa.Providers.WorkflowStorage;
using Elsa.Services;
using Elsa.Services.Models;
using Hangfire;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Workflows.Activities
{
    public record UpdateBlockchainContext(string WorkflowInstanceId, string ActivityId, string FileSignature);

    [Activity(Category = "Document Management", Description = "Saves a hash of the specified file onto the blockchain to prevent tampering.")]
    public class UpdateBlockchain : Activity
    {
        [ActivityInput(
            Label = "File",
            Hint = "The file to store its hash onto the blockchain. Can be byte[] or Stream.",
            SupportedSyntaxes = new[] { SyntaxNames.JavaScript, SyntaxNames.Liquid },
            DefaultWorkflowStorageProvider = TransientWorkflowStorageProvider.ProviderName
            )]
        public object File { get; set; } = default!;

        [ActivityOutput(
            Hint = "The computed file signature as stored on the blockchain."
            )]
        public string Output { get; set; }

        private readonly IBackgroundJobClient _backgroundJobClient;
        private readonly IWorkflowInstanceDispatcher _workflowInstanceDispatcher;

        public UpdateBlockchain(IBackgroundJobClient backgroundJobClient, IWorkflowInstanceDispatcher workflowInstanceDispatcher)
        {
            _backgroundJobClient = backgroundJobClient;
            _workflowInstanceDispatcher = workflowInstanceDispatcher;
        }

        public override async ValueTask<IActivityExecutionResult> ExecuteAsync(ActivityExecutionContext context)
        {
            var bytes = File is Stream stream
                ? await stream.ReadBytesToEndAsync()
                : File is byte[] buffer
                    ? buffer
                    : throw new NotSupportedException();

            var fileSignature = ComputeSignature(bytes);

            _backgroundJobClient.Enqueue(() => SubmitToBlockChainAsync(new UpdateBlockchainContext(context.WorkflowInstance.Id, context.ActivityId, fileSignature), CancellationToken.None));

            return Suspend();
        }

        /// <summary>
        /// Invoked by Hangfire as a background job.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        public async Task SubmitToBlockChainAsync(UpdateBlockchainContext context, CancellationToken cancellationToken)
        {
            //simulate storing file signature on an imaginary blockchain out there.
            await Task.Delay(TimeSpan.FromSeconds(15), cancellationToken);

            await _workflowInstanceDispatcher.DispatchAsync(new ExecuteWorkflowInstanceRequest(context.WorkflowInstanceId, context.ActivityId, new WorkflowInput(context.FileSignature)), cancellationToken);
        }

        protected override IActivityExecutionResult OnResume(ActivityExecutionContext context)
        {
            var fileSignature = context.GetInput<string>();

            Output = fileSignature;

            return Done();
        }

        private string ComputeSignature(byte[] bytes)
        {
            using var algorithm = SHA256.Create();
            var hashValue = algorithm.ComputeHash(bytes);
            return Convert.ToBase64String(hashValue);
        }
    }
}