﻿using Docuit.Core.Models;
using Docuit.Core.Services;
using Elsa.ActivityResults;
using Elsa.Attributes;
using Elsa.Expressions;
using Elsa.Providers.WorkflowStorage;
using Elsa.Services;
using Elsa.Services.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Docuit.Workflows.Activities
{
    public record DocumentFile(Document Document, Stream FileStream);

    [Action(
        Category = "Document Management",
        Description = "Gets the specified document from the database"
    )]
    public class GetDocument : Activity
    {
        [ActivityInput(
            Label = "Document ID",
            Hint = "The ID of the document to load",
            SupportedSyntaxes = new[] { SyntaxNames.JavaScript, SyntaxNames.Liquid }
        )]
        public string DocumentId { get; set; } = default!;

        [ActivityOutput(
            Hint = "The document + file",
            DefaultWorkflowStorageProvider = TransientWorkflowStorageProvider.ProviderName)]
        public DocumentFile Output { get; set; } = default!;

        private readonly IDocumentStore _documentStore;
        private readonly IFileStorage _fileStorage;

        public GetDocument(IDocumentStore documentStore, IFileStorage fileStorage)
        {
            _documentStore = documentStore;
            _fileStorage = fileStorage;
        }

        public override async ValueTask<IActivityExecutionResult> ExecuteAsync(ActivityExecutionContext context)
        {
            var document = await _documentStore.GetAsync(DocumentId, context.CancellationToken);
            var fileStream = await _fileStorage.ReadAsync(document!.FileName, context.CancellationToken);

            Output = new DocumentFile(document, fileStream);
            return Done();
        }
    }
}