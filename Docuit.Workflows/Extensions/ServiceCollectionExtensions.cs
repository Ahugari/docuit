﻿using Docuit.Workflows.Activities;
using Docuit.Workflows.Handlers;
using Docuit.Workflows.Scripting.Javascript;
using Elsa;
using Elsa.Persistence.EntityFramework.Core.Extensions;
using Elsa.Providers.Workflows;
using Elsa.Server.Hangfire.Extensions;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Storage.Net;
using System;
using System.IO;

namespace Docuit.Workflows.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddWorkflowServices(this IServiceCollection services, Action<DbContextOptionsBuilder> configureDb)
        {
            services.AddNotificationHandlersFrom<StartDocumentWorkflow>();
            services.AddElsa(configureDb);

            //Registering custom type definition provider for JS intellisense
            services.AddJavaScriptTypeDefinitionProvider<CustomTypeDefinitionProvider>();

            services.AddSingleton<IContentTypeProvider, FileExtensionContentTypeProvider>();

            return services;
        }

        public static IServiceCollection AddElsa(this IServiceCollection services, Action<DbContextOptionsBuilder> configureDb)
        {
            services.AddElsa(options => options
            .UseEntityFrameworkPersistence(configureDb)
            .AddConsoleActivities()
            .UseHangfireDispatchers()
            .AddEmailActivities()
            .AddHttpActivities()
            .AddActivitiesFrom<GetDocument>()
            );

            var currentAssemblyPath = Path.GetDirectoryName(typeof(ServiceCollectionExtensions).Assembly.Location);

            services.Configure<BlobStorageWorkflowProviderOptions>(options => options.BlobStorageFactory = () => StorageFactory.Blobs.DirectoryFiles(Path.Combine(currentAssemblyPath, "Workflows")));

            return services;
        }
    }
}