﻿using Docuit.Core.Events;
using Elsa.Models;
using Elsa.Services;
using MediatR;
using Open.Linq.AsyncExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Workflows.Handlers
{
    public class StartDocumentWorkflow : INotificationHandler<NewDocumentReceived>
    {
        private readonly IWorkflowRegistry _workflowRegistry;
        private readonly IWorkflowDefinitionDispatcher _workflowDispatcher;

        public StartDocumentWorkflow(IWorkflowRegistry workflowRegistry, IWorkflowDefinitionDispatcher workflowDispatcher)
        {
            _workflowRegistry = workflowRegistry;
            _workflowDispatcher = workflowDispatcher;
        }

        public async Task Handle(NewDocumentReceived notification, CancellationToken cancellationToken)
        {
            var document = notification.Document;
            var documentTypeId = document.DocumentTypeId;

            var workflows = await _workflowRegistry.FindManyAsync(x => x.Tag == documentTypeId && x.IsPublished, cancellationToken).ToList();

            if (workflows.Count < 1)
                return;

            foreach (var workflow in workflows)
            {
                await _workflowDispatcher.DispatchAsync(new ExecuteWorkflowDefinitionRequest(workflow!.Id, CorrelationId: document.Id, Input: new WorkflowInput(document.Id)), cancellationToken);
            }
        }
    }
}