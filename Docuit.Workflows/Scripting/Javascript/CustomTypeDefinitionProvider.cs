﻿using Docuit.Workflows.Activities;
using Elsa.Scripting.JavaScript.Services;
using System;
using System.Collections.Generic;

namespace Docuit.Workflows.Scripting.Javascript
{
    public class CustomTypeDefinitionProvider : TypeDefinitionProvider
    {
        public override IEnumerable<Type> CollectTypes(TypeDefinitionContext context)
        {
            yield return typeof(DocumentFile);
        }
    }
}