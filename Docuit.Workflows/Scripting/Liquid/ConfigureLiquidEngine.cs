﻿using Docuit.Core.Models;
using Docuit.Workflows.Activities;
using Elsa.Scripting.Liquid.Messages;
using Fluid;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Workflows.Scripting.Liquid
{
    public class ConfigureLiquidEngine : INotificationHandler<EvaluatingLiquidExpression>
    {
        public Task Handle(EvaluatingLiquidExpression notification, CancellationToken cancellationToken)
        {
            var memberAccessStrategy = notification.TemplateContext.Options.MemberAccessStrategy;

            memberAccessStrategy.Register<Document>();
            memberAccessStrategy.Register<DocumentFile>();

            return Task.CompletedTask;
        }
    }
}