﻿using Docuit.Core.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Core.Services
{
    public interface IDocumentTypeStore
    {
        Task<IEnumerable<DocumentType>> ListAsync(CancellationToken cancellationToken = default);

        Task<DocumentType?> GetType(string Id, CancellationToken cancellationToken = default);
    }
}