﻿using Docuit.Core.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Core.Services
{
    public interface IDocumentStore
    {
        Task SaveAsync(Document entity, CancellationToken cancellationToken = default);

        Task<Document> GetAsync(string Id, CancellationToken cancellationToken = default);
    }
}