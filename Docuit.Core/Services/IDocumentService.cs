﻿using Docuit.Core.Models;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Core.Services
{
    public interface IDocumentService
    {
        Task<Document> SaveDocumentAsync(string fileName, Stream data, string documentTypeId, CancellationToken cancellationToken = default);

    }
}