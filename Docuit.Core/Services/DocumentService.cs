﻿using Docuit.Core.Events;
using Docuit.Core.Models;
using MediatR;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Core.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IFileStorage _fileStorage;
        private readonly IDocumentStore _documentStore;
        private readonly IMediator _mediator;
        private readonly ISystemClock _systemClock;

        public DocumentService(IFileStorage fileStorage, IDocumentStore documentStore, IMediator mediator, ISystemClock systemClock)
        {
            _fileStorage = fileStorage;
            _documentStore = documentStore;
            _mediator = mediator;
            _systemClock = systemClock;
        }

        public async Task<Document> SaveDocumentAsync(string fileName, Stream data, string documentTypeId, CancellationToken cancellationToken = default)
        {
            await _fileStorage.WriteAsync(data, fileName, cancellationToken);

            var document = new Document
            {
                Id = Guid.NewGuid().ToString("N"),
                Status = DocumentStatus.New,
                DocumentTypeId = documentTypeId,
                CreatedAt = _systemClock.UtcNow,
                FileName = fileName
            };

            await _documentStore.SaveAsync(document, cancellationToken);

            await _mediator.Publish(new NewDocumentReceived(document), cancellationToken);

            return document;
        }
    }
}