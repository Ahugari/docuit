﻿using System;

namespace Docuit.Core.Services
{
    public interface ISystemClock
    {
        DateTime UtcNow { get; }
    }
}