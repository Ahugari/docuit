﻿using Microsoft.Extensions.Options;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Storage.Net.Blobs;
using Docuit.Core.Options;

namespace Docuit.Core.Services
{
    public class FileStorage : IFileStorage
    {
        private readonly IBlobStorage _blobStorage;

        public FileStorage(IOptions<DocumentStorageOptions> options)
        {
            _blobStorage = options.Value.BlobStorageFactory();
        }

        public Task<Stream> ReadAsync(string fileName, CancellationToken cancellationToken = default)
        {
            return _blobStorage.OpenReadAsync(fileName, cancellationToken);
        }

        public Task WriteAsync(Stream data, string fileName, CancellationToken cancellationToken = default)
        {
            return _blobStorage.WriteAsync(fileName, data, false, cancellationToken);
        }
    }
}