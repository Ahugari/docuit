﻿using Docuit.Core.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Docuit.Core.Events
{
    public record NewDocumentReceived(Document Document) : INotification;
}