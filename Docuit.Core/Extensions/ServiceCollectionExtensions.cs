﻿using Docuit.Core.Options;
using Docuit.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using Storage.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Docuit.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.Configure<DocumentStorageOptions>(options => options.BlobStorageFactory = StorageFactory.Blobs.InMemory);

            return services
                .AddSingleton<ISystemClock, SystemClock>()
                .AddSingleton<IFileStorage, FileStorage>()
                .AddScoped<IDocumentService, DocumentService>();
        }
    }
}