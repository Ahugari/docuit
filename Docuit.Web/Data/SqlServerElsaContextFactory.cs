﻿//using Elsa.Persistence.EntityFramework.Core;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Design;
//using Microsoft.Extensions.Configuration;
//using System.IO;

//namespace Docuit.Web.Data
//{
//    public class SqlServerElsaContextFactory : IDesignTimeDbContextFactory<ElsaContext>
//    {
//        public ElsaContext CreateDbContext(string[] args)
//        {
//            //var configuration = new ConfigurationBuilder()
//            //    .SetBasePath(Directory.GetCurrentDirectory())
//            //    .AddCommandLine(args)
//            //    .Build();

//            var dbContextBuilder = new DbContextOptionsBuilder<ElsaContext>();

//            //TODO: Should use env variable to set the connection string so it can be accessed in the commandline/powershell
//            var connectionString = "Server=THE-UNRULY\\SQLEXPRESS;Database=Docuit;Trusted_Connection=True;";

//            dbContextBuilder.UseSqlServer(connectionString, sqlServer => sqlServer
//                    .MigrationsAssembly(typeof(SqlServerElsaContextFactory).Assembly.GetName().Name)
//                    .MigrationsHistoryTable(ElsaContext.MigrationsHistoryTable, ElsaContext.ElsaSchema));

//            return new ElsaContext(dbContextBuilder.Options);
//        }
//    }
//}