﻿using Docuit.Core.Services;
using Docuit.Web.Models;
using Docuit.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Open.Linq.AsyncExtensions;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Docuit.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IDocumentTypeStore _documentTypeStore { get; }
        private IDocumentService _documentService { get; }
        private IDocumentStore _documentStore { get; }
        private FormViewModel _formViewModel { get; } = new FormViewModel();

        public HomeController(ILogger<HomeController> logger, IDocumentService documentService, IDocumentTypeStore documentTypeStore, IDocumentStore documentStore)
        {
            _logger = logger;
            _documentService = documentService;
            _documentTypeStore = documentTypeStore;
            _documentStore = documentStore;
        }

        public async Task<IActionResult> Index(CancellationToken cancellationToken)
        {
            var documentTypes = await _documentTypeStore.ListAsync(cancellationToken).ToList();

            _formViewModel.DocumentTypes = documentTypes.Select(x => new SelectListItem(x.Name, x.Id)).ToList();

            return View(_formViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Index(CancellationToken cancellationToken, FormViewModel formViewModel)
        {
            var extension = Path.GetExtension(formViewModel.FileUpload.FileName);
            var fileName = $"{Guid.NewGuid()}.{extension}";
            var fileStream = formViewModel.FileUpload.OpenReadStream();
            var document = await _documentService.SaveDocumentAsync(fileName, fileStream, formViewModel.DocumentTypeId, cancellationToken);

            return RedirectToAction("FileReceived", new { DocumentId = document.Id });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> FileReceived(CancellationToken cancellationToken, string DocumentId)
        {
            var document = await _documentStore.GetAsync(DocumentId, cancellationToken);

            if (document == null)
                return NotFound();

            var documentView = new DocumentViewModel();

            documentView.DocumentId = document.Id;

            return View(documentView);
        }

        [Route("/leave-request-approved")]
        public IActionResult RequestApproved(CancellationToken cancellation)
        {
            return View("LeaveRequestApproved");
        }

        [Route("/leave-request-denied")]
        public IActionResult RequestDenied(CancellationToken cancellation)
        {
            return View("LeaveRequestDenied");
        }
    }
}