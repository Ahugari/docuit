using Docuit.Core.Extensions;
using Docuit.Core.Options;
using Docuit.Infrastructure.Extensions;
using Docuit.Web.Data;
using Docuit.Workflows.Extensions;
using Elsa;
using Elsa.Activities.Email.Options;
using Elsa.Activities.Http.Options;
using Elsa.Server.Hangfire.Extensions;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NodaTime;
using NodaTime.Serialization.JsonNet;
using Storage.Net;
using System;
using System.IO;

namespace Docuit.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();

            services.AddRazorPages();

            AddHangfire(services, connectionString);

            AddWorkflowServices(services, connectionString);

            services.AddCors(cors => cors.AddDefaultPolicy(policy => policy
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowAnyOrigin()
                .WithExposedHeaders("Content-Disposition"))
            );

            services.AddNotificationHandlersFrom<Startup>();

            AddDomainServices(services);
            AddPersistenceServices(services, connectionString);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseHttpActivities();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }

        private void AddHangfire(IServiceCollection services, string connectionString)
        {
            services.AddHangfire(config => config
            .UseSqlServerStorage(connectionString)
            .UseSimpleAssemblyNameTypeSerializer()
            .UseRecommendedSerializerSettings(settings => settings.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb)))

                .AddHangfireServer((sp, options) =>
                {
                    Configuration.GetSection("Hangfire").Bind(options);

                    options.ConfigureForElsaDispatchers(sp);
                });
        }

        private void AddWorkflowServices(IServiceCollection services, string connectionString)
        {
            services.AddWorkflowServices(dbContext => dbContext.UseSqlServer(connectionString));

            services.Configure<SmtpOptions>(options => Configuration.GetSection("Elsa:Smtp").Bind(options));

            services.Configure<HttpActivityOptions>(options => Configuration.GetSection("Elsa:Server").Bind(options));

            services.AddElsaApiEndpoints();
        }

        private void AddDomainServices(IServiceCollection services)
        {
            services.AddDomainServices();

            services.Configure<DocumentStorageOptions>(options => options.
                BlobStorageFactory = () => StorageFactory
                    .Blobs
                    .DirectoryFiles(Path.Combine(Environment.CurrentDirectory, "App_Data/Uploads")));
        }

        private void AddPersistenceServices(IServiceCollection services, string connectionString)
        {
            services.AddDomainPersistence(connectionString);
        }
    }
}