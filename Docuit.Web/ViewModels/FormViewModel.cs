﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Docuit.Web.ViewModels
{
    public class FormViewModel
    {
        public FormViewModel() => DocumentTypes = new List<SelectListItem>();

        public string DocumentTypeId { get; set; }
        public IFormFile FileUpload { get; set; }
        public ICollection<SelectListItem> DocumentTypes { get; set; }
    }
}