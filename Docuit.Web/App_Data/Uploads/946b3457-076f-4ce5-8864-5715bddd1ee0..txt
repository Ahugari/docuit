Institutions
[Missing Districts]
    {
        "ID": 10,
        "Institution_Name": "Nyabyeya Forestry College, Masindi",
        "Award_Type": "OTI – Registered",
        "Accredit_Date": "1948-01-02T00:00:00",
        "License_No": "PB/OTREG/1948/000003",
        "District": null,
        "Type": "Public"
    },
    {
        "ID": 11,
        "Institution_Name": "Nsamizi Training Institute of Social Development",
        "Award_Type": "OTI – Registered",
        "Accredit_Date": "1952-01-02T00:00:00",
        "License_No": "PB/OTREG/1952/000067",
        "District": null,
        "Type": "Public"
    },
    {
        "ID": 18,
        "Institution_Name": "Mbale School of Midwifery and Nursing",
        "Award_Type": "OTI – Registered",
        "Accredit_Date": "1958-01-02T00:00:00",
        "License_No": "PB/OTREG/1958/000111",
        "District": null,
        "Type": "Public"
    },
    {
        "ID": 20,
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "Award_Type": "OTI – Registered",
        "Accredit_Date": "1965-01-02T00:00:00",
        "License_No": "PB/OTREG/1965/000109",
        "District": null,
        "Type": "Public"
    },
    {
        "ID": 29,
        "Institution_Name": "Uganda Institute of Allied Health and Management Science (UIAHMS) Mulago",
        "Award_Type": "OTI – Registered",
        "Accredit_Date": "1979-01-02T00:00:00",
        "License_No": "PB/OTREG/1979/000005",
        "District": null,
        "Type": "Public"
    },
    {
        "ID": 179,
        "Institution_Name": "Uganda Martyrs Seminary, Namugongo",
        "Award_Type": "Provisional License to Operate as Other Territory Institution",
        "Accredit_Date": "2014-12-19T00:00:00",
        "License_No": "PR/OTCR/2014/000009",
        "District": null,
        "Type": "Private"
    },
    {
        "ID": 206,
        "Institution_Name": "Sai Pali Institute of Technology & Management",
        "Award_Type": "Certificate of Classification and Registration",
        "Accredit_Date": "2016-03-30T00:00:00",
        "License_No": "PR/OTCR/2016/000013",
        "District": null,
        "Type": "Private"
    },
    {
        "ID": 207,
        "Institution_Name": "Uganda Baptist Seminary",
        "Award_Type": "Provisional License to Operate as Other Degree Awarding Institution",
        "Accredit_Date": "2016-03-30T00:00:00",
        "License_No": "PR/ODPL/2016/000010",
        "District": null,
        "Type": "Private"
    },
    {
        "ID": 240,
        "Institution_Name": "Northern Institute of Business Studies (NIBS)",
        "Award_Type": "Provisional License to Operate as Other Territory Institution",
        "Accredit_Date": "2018-05-28T00:00:00",
        "License_No": "PR/OTPL/2018/000123",
        "District": null,
        "Type": "Private"
    }








Programmes
[Missing Program Level]
    {
        "ID": 1587,
        "Program_Name": "ENTREPRENEURSHIP SUBJECT",
        "Accredited_Date": "2012-06-21T00:00:00",
        "Expiry_Date": "2017-06-21T00:00:00",
        "Institution_Name": "Islamic University in Uganda",
        "District": "Mbale",
        "Program_Level": null
    },
    {
        "ID": 2431,
        "Program_Name": "E4 Impact MBA in Impact Entrepreneurship",
        "Accredited_Date": "2015-09-29T00:00:00",
        "Expiry_Date": "2020-09-29T00:00:00",
        "Institution_Name": "Uganda Martyrs University",
        "District": "Mpigi",
        "Program_Level": null
    },
    {
        "ID": 2772,
        "Program_Name": "Introductory Physics for Life and Biomedical Science",
        "Accredited_Date": "2016-06-27T00:00:00",
        "Expiry_Date": "2021-06-27T00:00:00",
        "Institution_Name": "Kampala School of Health Sciences",
        "District": "Kampala",
        "Program_Level": null
    }



[Missing District]

    {
        "ID": 1051,
        "Program_Name": "Advanced Diploma in Theology Upgrade",
        "Accredited_Date": "2010-03-26T00:00:00",
        "Expiry_Date": "2015-03-26T00:00:00",
        "Institution_Name": "Uganda Baptist Seminary",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1052,
        "Program_Name": "Advanced Diploma in Theology ",
        "Accredited_Date": "2010-03-26T00:00:00",
        "Expiry_Date": "2015-03-26T00:00:00",
        "Institution_Name": "Uganda Baptist Seminary",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1053,
        "Program_Name": "Diploma in Theology Upgrade",
        "Accredited_Date": "2010-03-26T00:00:00",
        "Expiry_Date": "2015-03-26T00:00:00",
        "Institution_Name": "Uganda Baptist Seminary",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1054,
        "Program_Name": "Diploma in Theology ",
        "Accredited_Date": "2010-03-26T00:00:00",
        "Expiry_Date": "2015-03-26T00:00:00",
        "Institution_Name": "Uganda Baptist Seminary",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1358,
        "Program_Name": "Diploma in Procurement and Logistics",
        "Accredited_Date": "2011-09-26T00:00:00",
        "Expiry_Date": "2016-09-26T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1359,
        "Program_Name": "Diploma in Accounting and Finance",
        "Accredited_Date": "2011-09-26T00:00:00",
        "Expiry_Date": "2016-09-26T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1360,
        "Program_Name": "Certificate in Library and Information Science",
        "Accredited_Date": "2011-09-26T00:00:00",
        "Expiry_Date": "2016-09-26T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Certficate "
    },
    {
        "ID": 1369,
        "Program_Name": "Diploma in Computer Technology",
        "Accredited_Date": "2011-12-19T00:00:00",
        "Expiry_Date": "2016-12-19T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1373,
        "Program_Name": "Diploma in Counseling and Guidance",
        "Accredited_Date": "2011-12-19T00:00:00",
        "Expiry_Date": "2016-12-19T00:00:00",
        "Institution_Name": "Nsamizi Training Institute of Social Development",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1488,
        "Program_Name": "DIPLOMA IN PROCUREMENT AND LOGISTICS MANAGEMENT (DPML)",
        "Accredited_Date": "2012-06-21T00:00:00",
        "Expiry_Date": "2017-06-21T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1489,
        "Program_Name": "DIPLOMA IN ACCOUNTING AND FINANCE",
        "Accredited_Date": "2012-06-21T00:00:00",
        "Expiry_Date": "2017-06-21T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 1878,
        "Program_Name": "Diploma in Business Administration and Management",
        "Accredited_Date": "2013-12-20T00:00:00",
        "Expiry_Date": "2018-12-20T00:00:00",
        "Institution_Name": "Uganda Martyrs Seminary, Namugongo",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2458,
        "Program_Name": "Diploma in Community Development and Management",
        "Accredited_Date": "2015-09-29T00:00:00",
        "Expiry_Date": "2020-09-29T00:00:00",
        "Institution_Name": "Uganda Martyrs Seminary, Namugongo",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2459,
        "Program_Name": "Diploma in Christian Ministries",
        "Accredited_Date": "2015-09-29T00:00:00",
        "Expiry_Date": "2020-09-29T00:00:00",
        "Institution_Name": "Uganda Martyrs Seminary, Namugongo",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2500,
        "Program_Name": "Bachelor of Theology",
        "Accredited_Date": "2015-12-21T00:00:00",
        "Expiry_Date": "2020-12-21T00:00:00",
        "Institution_Name": "Uganda Baptist Seminary",
        "District": null,
        "Program_Level": "Bachelors"
    },
    {
        "ID": 2746,
        "Program_Name": "Diploma in Multimedia Technology",
        "Accredited_Date": "2016-06-27T00:00:00",
        "Expiry_Date": "2021-06-27T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2747,
        "Program_Name": "Diploma in Business Process Outsourcing Management",
        "Accredited_Date": "2016-06-27T00:00:00",
        "Expiry_Date": "2021-06-27T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2817,
        "Program_Name": "Diploma in Agro-Forestry",
        "Accredited_Date": "2016-12-19T00:00:00",
        "Expiry_Date": "2021-12-19T00:00:00",
        "Institution_Name": "Nyabyeya Forestry College, Masindi",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2818,
        "Program_Name": "Diploma in Bee Keeping",
        "Accredited_Date": "2016-12-19T00:00:00",
        "Expiry_Date": "2021-12-19T00:00:00",
        "Institution_Name": "Nyabyeya Forestry College, Masindi",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2819,
        "Program_Name": "Diploma in Forestry",
        "Accredited_Date": "2016-12-19T00:00:00",
        "Expiry_Date": "2021-12-19T00:00:00",
        "Institution_Name": "Nyabyeya Forestry College, Masindi",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2820,
        "Program_Name": "Diploma in Biomass Energy Technology",
        "Accredited_Date": "2016-12-19T00:00:00",
        "Expiry_Date": "2021-12-19T00:00:00",
        "Institution_Name": "Nyabyeya Forestry College, Masindi",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 2903,
        "Program_Name": "Diploma in Electrical and Electronics Engineering",
        "Accredited_Date": "2016-12-19T00:00:00",
        "Expiry_Date": "2021-12-19T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3108,
        "Program_Name": "Diploma in Business Administration",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3109,
        "Program_Name": "Diploma in Infromation Technology Business",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3110,
        "Program_Name": "Diploma in Computer Technology",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3111,
        "Program_Name": "Diploma in Records and Archives Management",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3112,
        "Program_Name": "Diploma in Accounting and Finance",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3113,
        "Program_Name": "Diploma in Procurement and Logistics Management",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3114,
        "Program_Name": "Diploma in Information Technology Science",
        "Accredited_Date": "2017-05-22T00:00:00",
        "Expiry_Date": "2022-05-22T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3639,
        "Program_Name": "Diploma in  Accounting and Finance",
        "Accredited_Date": "2018-05-28T00:00:00",
        "Expiry_Date": "2023-05-28T00:00:00",
        "Institution_Name": "Northern Institute of Business Studies (NIBS)",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3640,
        "Program_Name": "Diploma in Business Administration",
        "Accredited_Date": "2018-05-28T00:00:00",
        "Expiry_Date": "2023-05-28T00:00:00",
        "Institution_Name": "Northern Institute of Business Studies (NIBS)",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3641,
        "Program_Name": "Diploma in Provcurement and Logistics",
        "Accredited_Date": "2018-05-28T00:00:00",
        "Expiry_Date": "2023-05-28T00:00:00",
        "Institution_Name": "Northern Institute of Business Studies (NIBS)",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 3672,
        "Program_Name": "Diploma in Public Administration and Management",
        "Accredited_Date": "2018-08-27T00:00:00",
        "Expiry_Date": "2023-08-27T00:00:00",
        "Institution_Name": "Northern Institute of Business Studies (NIBS)",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 4266,
        "Program_Name": "Higher Education Certificate- Physical Science",
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Higher Education Certificate"
    },
    {
        "ID": 4267,
        "Program_Name": "Higher Education Certificate- Humanities (Business)",
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Higher Education Certificate"
    },
    {
        "ID": 4268,
        "Program_Name": "Higher Education Certificate- Humanities (Library and Information Science )",
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Higher Education Certificate"
    },
    {
        "ID": 4302,
        "Program_Name": "Higher Education Certificate - Physical Sciences",
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Higher Education Certificate"
    },
    {
        "ID": 4303,
        "Program_Name": "Higher Education Certificate - Humanities (Business)",
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Higher Education Certificate"
    },
    {
        "ID": 4304,
        "Program_Name": "Higher Education Certificate - Humanities (Library & Information Science)",
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Uganda Institute of Information and Communications Technology",
        "District": null,
        "Program_Level": "Higher Education Certificate"
    },
    {
        "ID": 4669,
        "Program_Name": "Bachelor of Arts in Biblical Studies",
        "Accredited_Date": "2020-12-14T00:00:00",
        "Expiry_Date": "2025-12-14T00:00:00",
        "Institution_Name": "African Bible University",
        "District": null,
        "Program_Level": "Bachelors"
    },
    {
        "ID": 4670,
        "Program_Name": "Bachelor of Business Administration ",
        "Accredited_Date": "2020-12-14T00:00:00",
        "Expiry_Date": "2025-12-14T00:00:00",
        "Institution_Name": "African Bible University",
        "District": null,
        "Program_Level": "Bachelors"
    },
    {
        "ID": 4671,
        "Program_Name": "Bachelor of Mass Communication ",
        "Accredited_Date": "2020-12-14T00:00:00",
        "Expiry_Date": "2025-12-14T00:00:00",
        "Institution_Name": "African Bible University",
        "District": null,
        "Program_Level": "Bachelors"
    },
    {
        "ID": 4842,
        "Program_Name": "Diploma in Software Engineering",
        "Accredited_Date": "2021-03-01T00:00:00",
        "Expiry_Date": "2026-03-01T00:00:00",
        "Institution_Name": "Sai Pali Institute of Technology & Management",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 4843,
        "Program_Name": "Diploma in Infrastructre and Management Services",
        "Accredited_Date": "2021-03-01T00:00:00",
        "Expiry_Date": "2026-03-01T00:00:00",
        "Institution_Name": "Sai Pali Institute of Technology & Management",
        "District": null,
        "Program_Level": "Diploma"
    },
    {
        "ID": 4844,
        "Program_Name": "Diploma in Visual Effects and Animation",
        "Accredited_Date": "2021-03-01T00:00:00",
        "Expiry_Date": "2026-03-01T00:00:00",
        "Institution_Name": "Sai Pali Institute of Technology & Management",
        "District": null,
        "Program_Level": "Diploma"
    }
]

[Missing Programme Name]
[
    {
        "ID": 4311,
        "Program_Name": null,
        "Accredited_Date": "2019-12-13T00:00:00",
        "Expiry_Date": "2024-12-13T00:00:00",
        "Institution_Name": "Limkokwing University of Creative Technology  ",
        "District": "Mukono",
        "Program_Level": "Bachelors"
    }
]